import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false
Vue.directive('preventReClick', {
  inserted (el, binding) {
    console.log(binding.value)
    if (!el.disabled) {
      el.addEventListener('click', () => {
        el.disabled = true
        const timer = setTimeout(() => {
          el.disabled = false
          clearTimeout(timer)
        }, binding.value)
      })
    }
  }
})
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
