module.exports = {
  configureWebpack:{
    resolve:{
      alias:{
        components:"@/components"
      }
    }
  },
  lintOnSave:false,//关闭eslint
  //如果开始创建项目没安装eslint,后面又想用,那就重新创建新项目,把eslint配置赋值到老项目,注意package.json和eslintrc.js都要复制
  devServer: {
    // overlay: { // 让浏览器 overlay 同时显示警告和错误
    //   warnings: true,
    //   errors: true
    // },
    open: true, // 是否打开浏览器
    
    host: "localhost",
    // port: "8080", // 代理断就
    // https: false,
    // hotOnly: true, // 热更新
    
    
  },
  configureWebpack:{
    devtool:'cheap-module-eval-source-map'
  }
};